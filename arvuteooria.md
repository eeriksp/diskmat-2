# Arvuteooria

## Jaguvus

Tähistus $a|b$ tähendab, et $a$ jagab $b$’d, ehk siis $\frac{b}{a}$ on täisarv, näiteks $2|6$

* Igal täisarvul $b$ on vähemalt neli jagajat: $1, -1, b, -b$.

* Iga täisarvu $b$ korral $b|0$
* $0|0$, $\frac{0}{0}$ on määramatus
* kui $a|b$ ja $a|c$, siis $a|(bx + xy)$, näide: $2|6$ ja $2|8$, seega $2|(6x+8y)$
  * kui arv $a$ jagab nii arvu $b$ kui $c$, siis jagab ta ka nende summat ja vahet

Jaguvuse omadused:

* reflektiivsus: $a|a$
* antisümmeetria:  $a|b$ ja $b|a$ saavad korraga kehtida siis ja ainult siis kui $a=b$ või $a=-b$.
* transitiivsus: $a|b$ ja $b|c \to a|c$

Jäägiga jaguvust (mittejaguvust) tähistatakse: $a\nmid b$

* $b=aq + r$, kus $0 \leq r < a$
  * $b$ – jagatav
  * $a$ – jagaja
  * $q$ – jagatis
  * $r$ – jääk

* jääk on alati jagajast väiksem
* jääk on alati mittenegatiivne, seega $-17 \div 3$, jääk on $+1$, sest $-17 = 3(-6) + 1$

## Algarvud

1 ja 0 ei ole ei algarvud ega kordarvud.

Kontrollimaks kas arv $n$ on algarv, tuleb vaadata, kas ta jagub mõne arvuga vahemikus 2 … $\sqrt{n}$.

**Eukleidese teoreem: algarve on lõpmata palju.**

* Tõestus: eeldame, et algarve on lõplik hulk ($p_1, p_2, p_3,..., p_k$), moodustame nüüd uue arvu $n=p_1p_2p_3...p_k + 1$. See arv ei jagu ühegi algarvuga, järjelikult ei jagu ta ka ühegi kordarvuga (sest iga kordarvu saab esitada algarvude korrutisena). Kuna arv $n$ jagub vaid iseenda ja ühega, on ta samuti algarv ja seega eeldus, et algarve on lõplik hulk, ei pidanud paika. $M.O.T.T.$

Iga positiivse täisarvu $k$ jaoks leidub $k$ järjestikkust kordarvu.

algarvufunktsioon $\pi(n)$ näitab, kui palju on hulgas {1,2,…,n} algarve.

  * $$
    \pi(n) \sim \frac{n}{\ln n}
    $$

* Iga ühest suuremat naturaalarvu saab parajasti ühel viisil esitada algarvude korrutisena

Arvu kanooniline kuju: $n=p_1^{a_1}p_2^{a_2}...p_k^{a_k}$, kus $p$ on arvu algarvuline tegur ja $a$ on astendaja. Näiteks $35=5 \cdot 7$, $600=2^3 \cdot 3 \cdot 5^2$

Arvu tegurite arvu leidmine: arvul $n=p_1^{a_1}p_2^{a_2}...p_k^{a_k}$ on $(a_1+1)(a_2+1)...(a_k+1)$ tegurit.

### Fermat’ väike teoreem

Kui $p$ on algarv, siis iga täisarvu $a$ korral, mis ei jagu $p$’ga, kehtib seos:
$$
p|a^{p-1}-1
$$
Võib panna kirja ka nii (sellisel juhul on $p$ algarv, $a$ jaguvus $p$’ga pole oluline):
$$
p|a^p -a
$$


<!-- NB! see on tähtis, tõestust küsitakse eksamil. -->

::: details Fermat’ väikse teoreemi tõestus

1. Kui $p$ on algarv ja $0 < k < p$, siis $p|\binom{p}{k}$, sest
   $$
   \binom{p}{k} = \frac{p!}{k!(p-k)!}
   $$
   Selles avaldises on lugejas sees $p$, kuna $p$ on algarv ja nimetajas on väiksemad arvud, siis mitte miski $p$’d ära ei taanda ja seega see murd jagub $p$’ga.

2. Tõestame teoreemi induktsiooni abil.

   1. Eeldus: $a=0$: $a^p - a = 0^p - 0 = 0$. Iga arv, ka $p$, jagab nulli.

   2. Samm: olgu $a > 0$ ja $a=b+1$
      $$
      \begin{aligned}
      a^p - a &= (b+1)^p - (b+1) = \\
      &= \sum_{k=0}^p \binom{p}{k} b^{p-k}1^k -(b+1)
      &&\text{ (binoomi teoreem)} \\
      &= \sum_{k=0}^p \binom{p}{k} b^{p-k} -b-1 \\
      &= \binom{p}{0} b^p + \binom{p}{1} b^{p-1} + ... + \binom{p}{p-1} b + \binom{p}{p} b^{p-p} -b-1
      && \binom{p}{0} = \binom{p}{p} = 1 \\
      &= b^p + \binom{p}{1} b^{p-1} + ... + \binom{p}{p-1} b + 1 -b-1 \\
      &= b^p + \binom{p}{1} b^{p-1} + ... + \binom{p}{p-1} b-b \\
      &= (b^p - b) + \binom{p}{1} b^{p-1} + ... + \binom{p}{p-1} b \\
      \end{aligned}
      $$
      $(b^p-b)$ jagub $p$’ga tulenevalt induktsiooni eeldusest. $\binom{p}{1} b^{p-1} + ... + \binom{p}{p-1} b$ jagub samuti, sest vastavalt tõestuse punktis 1 toodud seosele jaguvad kõik need binoomkordajad $p$’ga, seega jaguvad ka kõik need korrutised, mis neid binoomkordajaid sisaldavad. Kõikide nende $p$’ga jaguvate arvude summa peab samuti $p$’ga jaguma. $M.O.T.T.$

:::

Pseudoalgarvud – kordarvud, mis rahuldavad Fermat’ väikest teoreemi vähemalt ühe $a$ korral. Näiteks 341 (teoreemis $p$), siis kui $a$ on 2.

Carmichaeli arvud – paaritud kordarvud, mis rahudavad Fermat’ väikest teoreemi iga $a$ korral.

## Suurim ühistegur

Naturaalarvude $a$ ja $b$ ühiseguriks nimetatakse naturaalarvu, millega jagub nii $a$ kui $b$, tähistatakse $SÜT(a,b)$ või $gcd(a.b)$ (*Greatest Common Divisor*).

Kui 
$$
\begin{aligned}
\text{kui} \\
a &= p_1^{a_1}p_2^{a_2}...p_k^{a_k} \\
b &= p_1^{b_1}p_2^{b_2}...p_k^{b_k} \\
\text{siis} \\
gcd(a, b) &= p_1^{min(a_1,b_1)}p_2^{min(a_2,b_2}...p_k^{min(a_k,b_k}
\end{aligned}
$$

### Suurima ühisteguri leidmine

Näide: leida suurim ühistegur arvudele 36 ja 60.

1. Viin esimese arvu kanoonilisele kujule: proovin seda jagada kõige väiksema algarvuga. Kirjutan arvu ühele poole joont ja kõige väiksema algarvu, mis seda arvu jagab teisele poole joont. Järgmisele reale kirjutan joonest paremale jagamistehte tulemuse ja korda sama sammu. Kordan samu samme teise arvuga.
   $$
   \begin{array}{c|c}
   36 & 2 \\
   18 & 2 \\
   9 & 3 \\
   3 & 3 \\
   1 & \\
   \end{array}
   \text{ }
   \begin{array}{c|c}
   60 & 2 \\
   30 & 2 \\
   15 & 3 \\
   5 & 5 \\
   1 & \\
   \end{array}
   $$
   
2. Saan, et $36 = 2 \cdot 2 \cdot 3 \cdot 3$ ja $60 = 2 \cdot 2 \cdot 3 \cdot 5$
3. SÜT on mõlema arvu ühite algtegurite korrutis: $2\cdot 2\cdot 3 = 12$

$gcd$ omadusi:

* $gcd(a,b)$ jabug iga $a$ ja $b$ ühiteguriga.
* $gcd(na,nb) = n \cdot gcd(a,b)$

## Eukleidese algoritm

Otsime kahe naturaalarvu $a$ ja $b$ suurimat ühistegurit, eeldame, et $a>b$

```
var a: int
var b: int
var r: int
while (b > 0) {
	r = a mod b
	a = b
	b = r
}
return a
```

Ehks siis

```
while(a != b) {
	if (a > b) {
		a = a - b
	} else {
		b = b - a
	}
}
return a // or `b` since in this point they are equal
```

Näide
$$
\begin{array}{c c}
\text{näide 1} \\
a   & b  \\
412 & 69 \\
69  & 5  \\
5   & 4  \\
4   & 1  \\
1   & 0  \\
gcd \text{ on } 1
\end{array}
\text{.......... }
\begin{array}{c c}
\text{näide 2} \\
a   & b  \\
340 & 16 \\
16  & 4  \\
4   & 0  \\
gcd \text{ on } 4
\end{array}
$$

## Bèzout’ lemma

Naturaalarvude $a$ ja $b$ suurimat ühistegurit saab avaldada kujul

$$
gcd(a,b) = sa + tb
$$

Kus $s$ ja $t$ on täisarvud.

::: details näide

Leida $gcd(360, 294)$
$$
\begin{array}{c c}
a   & b  \\
360 & 294 \\
294 & 66  \\
66  & 30  \\
30  & 6  \\
6   & 0  \\
gcd \text{ on } 6
\end{array}
$$

$$
\begin{align*}
    6 &= 66 - 2\cdot30 && \\
    6 &= 66 - 2(294-4\cdot66) && (30=294-4\cdot66) \\
    6 &= (360-1\cdot294) - 2(294-4(360-1\cdot294)) && (66=360-1\cdot294) \\
    6 &= (360-294) - 2(294-4(360-294)) \\
    6 &= 360-294 - 2\cdot294+8(360-294) \\
    6 &= 360-294 - 2\cdot294+8\cdot360-8\cdot294 \\
    6 &= \pmb{9}\cdot360-\pmb{11}\cdot294\\
    
\end{align*}
$$

:::

## Lineaarsed diofantilised võrrandid

Diofantiline võrrand on mitme tundmatuga täisarvuliste kordajatega algebraline võrrand, millele otsitakse täisarvulist lahendit.

Lineaarsel diofantilisel võrrandil
$$
ax + by = c
$$
Leiduvad täisarvulised lahendid parajasti siis, kui $gcd(a,b)|c$.

Lahendamine: 

**(1)** Ühe erilahendi leidmine: Eukleidese algoritmist leida $s$ ja $t$ nii, et $sa + tb = gcd(a,b)$ ning seejärel arvutada lahendid
$$
\begin{cases}
x = \frac{cs}{gcd(a,b)} \\
y = \frac{ct}{gcd(a,b)} \\
\end{cases}
$$

::: details Tõestus
$$
a\frac{cs}{gcd(a,b)} + b\frac{ct}{gcd(a,b)} = c \\
csa + ctb = c \cdot gcd(a,b)\\
sa + tb = gcd(a,b)
$$
:::

**(2)** Üldlahend avaldub kujul
$$
\begin{cases}
x = x_0 + \frac{kb}{gcd(a,b)} \\
y = y_0 + \frac{ka}{gcd(a,b)} \\
\end{cases}
$$
Kus $k$ on suvaline täisarv ja $x_0$, $y_0$ on erilahendid, mis leiti esimeses punktis.
NB! Siin $x$ valemis on $b$ ja $y$ valemis on $a$, ehk siis vastupidi kui esialgses võrrandis.

## Aritmeetika põhiteoreem

Iga positiivne täisarv on üheselt esitatav algarvude korrutisena.

## Moodularitmeetika

Arvud $a$ ja $b$ on **kongruentsed** mooduli $m$ järgi (m > 0) kui nad annavad jagamisel $m$-iga sama jäägi. Tähistus $a \equiv b (\mod m)$, eks siis $m|(b-a)$.

Kongruentsus on ekvivalentsi seos, sest ta on reflektiivne, sümmeetriline ja transitiivne, seega tekitab ta hulga peal klassijaotuse (ehk siis jagab kõik hulga liikmed üksteisega mitte kattuvatesse gruppidesse).

Kongruentsi omadusi:
$$
\def\s{\space}
a \equiv b \s \& \s c \equiv d \rightarrow ac \equiv bd \\
a \equiv b \s \& \s c \equiv d \rightarrow a+c \equiv b+d \\
a \equiv b  \rightarrow a^m \equiv b^m \\
$$




### Jagamistehe moodularitmeetikas

Moodularitmeetikas on jagamine üheselt määratud prasjasti siis, kui moodul on algarv.

Moodularitmeetikas joonistatatakse thtede peale kriips, et näidata, et tegemist on ühe selle arvuklassi esinadajaga, aga selle asemel võiks olla ka mõni muu samast klassist arv. Näiteks moodul 10 puhul viitab $\bar{1}$ kõikidele arvudele, mis annavad 10-ga jagamisel jäägiks 1 (1, 11, 21, 31, 41 …).

Leida $\bar{x} = \bar{a}/\bar{b} \space (\mod p)$. $p$ on algarv.

**(1)** leida $\bar{y}=\bar{1}/\bar{b}$

Leida Eukleidese algoritmi abil $gcd(p,b)=1$ (vastus tuleb alati 1, sest $p$ on algarv).

Bèzout’ lemma järgi saab suurimat ühistegurit kirjutada nii $sp + tb = gcd(a,b)$, leida kordajad $s$ ja $t$, saame, et $sp + tb=1$.

Leiame jäägid, mis tekivad  saadud avaldise läbi jagamisel mooduliga $p$: $\bar{t}\bar{b} = \bar{1} \to \bar{t}=\bar{1}/\bar{b} = \bar{y}$

**(2)** leida $\bar{x}=\bar{y} \cdot \bar{b}$

::: details Näide

Leida $\overline{x} = \overline{-3}/\overline{7} \space (\mod 47)$

1. Leida $\overline{y}=\overline{1}/\overline{7}$

   Eukleidese algoritmist
   $$
   \begin{array}{c c}
   47 & 7 \\
   7 & 5 \\
   5 & 2 \\
   2 & 1 \\
   1 & 0 \\
   \end{array}
   $$

   $$
   \begin{align*}
       1 &= 2 - 1 \\
       1 &= 2 - (5-2\cdot2) \\
       1 &= 2 - 5+2\cdot2) && (1=5-2\cdot2) \\
       1 &= 7-5 - 5+2\cdot(7-5) && (2=7-5) \\
       1 &= 7-5 - 5+2\cdot7-2\cdot5 \\
       1 &= 3\cdot7-4\cdot5 \\
       1 &= 3\cdot7-4(47-6\cdot7) && (5=47-6\cdot7) \\
       1 &= 3\cdot7-4\cdot47+24\cdot7 \\
       1 &= \pmb{27}\cdot7\pmb{-4}\cdot47 \\    
   \end{align*}
   $$

   $$
   \text{leiame iga liikme jäägi, mis tekib jagamisel mooduliga 47.} \\
   \overline{1} = \overline{27}\cdot\overline{7} \\
   \frac{\overline{1}}{\overline{7}} = \overline{27}
   $$

   2. Leida $\overline{x} = \overline{-3}\cdot\frac{\overline{1}}{\overline{7}} = \overline{-3}\cdot\overline{27} = \overline{−81} = \overline{−81}+2\cdot47=\overline{13}$

      Eelmisel real on -81-st 13 saamisel rakendatud omadust, et moodulit v]ib alati juurde liita või lahutada. 

:::

### Hiina järjekorra teoreem

Kui $m_1$, $m_2$, , …$m_n$ on paarikaupa ühistegurita positiivsed täisarvud, siis leidub kongruentside süsteemil
$$
\begin{cases}
x \equiv c_1 (\mod m_1) \\
x \equiv c_2 (\mod m_2) \\
... \\
x \equiv c_3 (\mod m_3)
\end{cases}
$$
parajasti üks lahend mooduli $M=m_1m_2…m_n$ järgi. See lahend avaldub kujul
$$
x=c_1M_1y_1 + c_2M_2y_2 + ... + c_nM_ny_n
$$
kus $M_n = \frac{M}{m_n}$ ja $y_n$ on $M_n$ on pöördväärtus mooduli $m_n$ järgi.

::: details Näide: Sun-Tsu mõistatus

Arvu $x$ jagamisel kolmega tekib jääk 2, jagamisel viiega tekib jääk 3, jagamisel seitsmega tekib jääk 2. Milline on see arv?
$$
\begin{cases}
x \equiv 2 \space (\mod 3) \\
x \equiv 3 \space (\mod 5) \\
x \equiv 2 \space (\mod 7)
\end{cases}
$$

$$
\begin{align*}
    M = 3\cdot5\cdot7 = 105 \\
    M_1 = 105 \div 3 = 35  && y_1=\frac{1}{\overline{M_1}} = \overline{2} \\
    M_2 = 105 \div 5 = 21 && y_2=\frac{1}{\overline{M_2}} = \overline{1} \\
    M_1 = 105 \div 7 = 15 && y_3=\frac{1}{\overline{M_3}} = \overline{1}\\
\end{align*}
$$

Vastavalt Hiina teoreemile:
$$
\def\d{\cdot}
\def\s{\space}
x = 2\d35\d2 + 3\d21\d1 + 2\d15\d1=233 \equiv 23 \s(\mod 105)\\
x = 23 + 105k,\s k \in \mathbb{Z}
$$
:::

## Algarvulisuse kontrollimine

**Fermat’ test**: Ferma’t teoreemist: kui $p$ on algarv ja $a$ on täisarv, kusjuures $1 \leq a \leq p$, siis 
$$
a^{p-1} \equiv 1 \space (\mod p)
$$
Kui see tingimus on rahuldatud kõikide $a$ väärtuste korral, võib väita, et $p$ on algarv.

Selle testi probleemiks on Carmichaeli arvud, mis antud testi küll läbivad, kui ei ole siiski päris algarvud, neid esineb küll väga vähe, aga siiski.

 Antud probleemi lahendab ära **Miller-Rabini test**.