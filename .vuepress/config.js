module.exports = {
    title: 'Matemaatika',
    dest: 'public',
    base: '/math/',
    themeConfig: {
      nav: [
        { text: 'Kombinatoorika', link: '/kombinatoorika/' }
        { text: 'Arvujadad', link: '/rekurrentsed-arvujadad/' },
        { text: 'Arvuteooria', link: '/arvuteooria/' }
      ]
    },
    markdown: {
        extendMarkdown: md => {
            md.use(require("markdown-it-katex"));
        }
    },
    head: [
        [
            "link",
            {
                rel: "stylesheet",
                href: "https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.5.1/katex.min.css"
            }
        ],
        [
            "link",
            {
                rel: "stylesheet",
                href: "https://cdn.jsdelivr.net/github-markdown-css/2.2.1/github-markdown.css"
            }
        ]
    ]
}
