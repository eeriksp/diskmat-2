# Rekurrentsed arvujadad

Arvujada on rekurrentne, kui selle liikmeid saab esitada eelmiste liikmete kaudu.
Üldkujul $a_n=g(a_{n-1}, ..., a_{n-k})$.
Valemis esinevate eelmiste iikmete arvu nimetatakse võrrandi järguks.

Näiteks naturaalarvude permutatsioonide jada on rekurrentne,
selle rekurrentne võrrand on $P_n=nP_{n-1}$ (esimest järku).
Võrrandi kuju, kus enam eelmisi liikemid ei esine (esineb ainult indeks $n$),
nimetatakse selle lahendiks ehk üldliikme kinniseks (analüütiliseks) esitluseks,
antud võrrandi puhul on see $P_n=n!$.

Arvujadade entsüklopeedia: <http://oeis.org/>



## Rekurrentse võrrandi lahendamine

Lahendada saab vaid väga lihtsaid rekurrentseid võrrandeid.

### Proovimise teel

1. arvutada välja mingi hulk jada liikmeid
2. proovida näha läbi mingi seos liikmete vahel
3. tõestada induktsiooni abil

### Iteratsiooni meetod

Hakata eelmist liiget võrrandisse asendama ja vaatama, millise loogika järgi võrrand muutuma hakkab.

::: details Näide: 

$Z(n) = aZ(n-1) +b$,  $Z(0) = c$

$$\begin{aligned}
Z(n)&= a[aZ(n-2)+b]+b \\
&=a^2 Z(n-2)+ab+b \\
&=a^2 [aZ(n-3)+b]+ab+b \\
&=a^3 Z(n-3)+a^2b+ab+b \\
&=a^k Z(n-k)+(a^{k-1}b + a^{k-2}b+ ... + a^0b) \\
&=a^k Z(n-k)+b(a^{k-1} + a^{k-2}+ ... + a^0) \\
&\text{tulenevalt geomeetrilise jada summa valemist,} \\
&\text{kus } a_1=1 \text{ ja jada tegur }q=a \\
&=a^k Z(n-k)+b \frac{a^k-1}{a-1} \\
kui \space k=n \\
&=a^n Z(n-n)+b \frac{a^n-1}{a-1} \\
&=\bold{a^n c + \frac{a^n-1}{a-1} b} \space (kui \space a \ne 1)\\
\end{aligned}$$

:::

### Karakteristlik võrrand lineaarsete rekurrentsete jadade jaoks

Kui rekurrentne võrrand on esitatav kujul

$a_n = s_0(n) + s_1 a_{n-1} + s_2 a_{n-2} +...+ s_k a_{n-k}$,

siis nimetatakse seda lineaarseks k-ndat järku rekurrentseks võrrandiks. Kui $s_0(n)=0$, (vabaliige puudub), on tegemist homogeense võrrandiga.

Lineaarsele homogeensele võrrandile, mis on antud kujul

$a_n + s_1 a_{n-1} + s_2 a_{n-2} +...+ s_k a_{n-k} = 0$

vastab karakteristlik võrrand $x^k + s_1x^{k-1} + s_2x^{k-2} + s_k=0$

**Lemma A**: kui arvujadad <$a_n$> ja  <$b_n$> on homogeense lineaarse võrrandi lahendid, siis on ka nende lineaarkombinatsioon <$c_1 a_n + c_2 b_n$> selle võrrandi lahend ($c_1$ ja $c_2$ on suvalised konstandid).

**Lemma B:** Kui $x_1$ on karakteristliku võrrandi $x^2 + px + q = 0$ lahend, siis on geomeetriline jada <$x_1^n$> võrrandi $a_n + pa_{n-1} + qa_{n-2} = 0$ lahendiks.

Võrrandi **üldlahendis** on sees määramata konstandid. Näiteks Fiboncci ja Lucas arvudel on sama üldlahend, aga erinevad esimesed liikmed.

#### Homogeensed lineaarsed võrrandid

Iga lahend on esitatav kujul:
1. $a_n = c_1 x_1^n + c_2 x_2^n$ kui $x_1 \ne x_2$
1. $a_n = (c_1 + c_2n) x_1^n$ kui $x_1 = x_2$

::: details Näide: Fibonacci jada võrrandi lahendamine

Võrrand: $F_n = F_{n-1} + F_{n-2}\text{, } F_0=0 \text{ ja } F_1=1$

seega $F_n - F_{n-1} - F_{n-2} = 0$

karakteristlik võrrand on $x_2 - x - 1 = 0$

$$
\begin{matrix}
x = -\frac{p}{2} \pm \sqrt{(\frac{p}{2})^2 - q} \\
x = \frac{1}{2} \pm \sqrt{\frac{1}{4} + 1}
= \frac{1}{2} \pm \sqrt{\frac{5}{4}}
= \frac{1}{2} \pm \frac{\sqrt{5}}{2} \cr
x_1 = \frac{1 + \sqrt{5}}{2} \text{ ; }
x_2 = \frac{1 - \sqrt{5}}{2}
\end{matrix}
$$


Seega Fibonacci arvude võrrandi lahend on

$$F_n = c_1 (\frac{1 + \sqrt{5}}{2})^n + c_2 (\frac{1 - \sqrt{5}}{2})^n$$

Leiame rajatingimused ($c_1$ ja $c_2$):
$$
\begin{matrix}
\begin{cases}
F_0 = c_1 (\frac{1 + \sqrt{5}}{2})^0 +
c_2 (\frac{1 - \sqrt{5}}{2})^0 = 0 \\
F_1 = c_1 (\frac{1 + \sqrt{5}}{2})^1 +
c_2 (\frac{1 - \sqrt{5}}{2})^1 = 1 \\
\end{cases} \\

\begin{cases}
c_1 + c_2 = 0 \\
c_1 (\frac{1 + \sqrt{5}}{2}) +
c_2 (\frac{1 - \sqrt{5}}{2}) = 1 \\
\end{cases} \\

c_2 = -c_1 \\
c_1 (\frac{1 + \sqrt{5}}{2}) -
c_1 (\frac{1 - \sqrt{5}}{2}) = 1 \\
c_1 (\frac{1 + \sqrt{5}}{2} - \frac{1 - \sqrt{5}}{2}) = 1 \\
c_1 (\frac{1 + \sqrt{5} - 1 + \sqrt{5}}{2}) = 1 \\
\sqrt{5}c_1  = 1 \\
c_1 = \frac{1}{\sqrt{5}} \text{ ; } c_2 = -\frac{1}{\sqrt{5}}
\end{matrix}
$$
Seega

$$F_n = \frac{1}{\sqrt{5}} (\frac{1 + \sqrt{5}}{2})^n -
\frac{1}{\sqrt{5}} (\frac{1 - \sqrt{5}}{2})^n$$
:::

#### Mittehomogeensed lineaarsed võrrandid

Mittehomogeense lineaarse võrrandi üldlahend on talle vastava homogeense võrraandi üldlahendi ning mingi erilahendi summa.

::: details Näide konstantse vabaliikmega:

**Lahendada võrrand $a_n = 6a_{n-1} - 8a_{n-2} + 9$ algtingimustel $a_0=10$ ja $a_1=25$.**

1. Talle vastava homogeense võrrandi ($a_n = 6a_{n-1} - 8a_{n-2}$) karakteristlik võrrand on $x^2 -6x +8 = 0$

2. Karakteristliku võrrandi lahendid on $x_1=2$, $x_2=4$

3. Leida erilahend (mingi jada, mis sobiks algse võrrandi lahendiks). Antud näites on vabaliige konstant (ei sõltu $n$’ist) ja seega sobib lahendiks mingi konstantne jada. Seega $a_n = 6a_n - 8a_n + 9 \implies 3a_n=9 \implies a_n=3$

4. Seega algse võrrandi üldlahend on $a_n = c_12^n + c_24^n + 3$

5. Algtingimustest saame võrrandsüsteemi $c_1$ ja $c_2$ leidmiseks
   $$
   \begin{matrix}
   \begin{cases}
   c_12^0 + c_24^0 + 3 = 10 \\
   c_12^1 + c_24^1 + 3 = 25
   \end{cases} \\
   \begin{cases}
   c_1 + c_2 = 7 \\
   2c_1 + 4c_2 = 22
   \end{cases} \\
   c_2 = 7 - c_1 \\
   2c_1 + 28 - 4c_1 = 22 \\
   -2c_1 = -6 \\
   c_1 = 3 \text{ ; } c_2 = 4
   \end{matrix}
   $$

6. Vastus etteantud tingimuste jaoks on seega $a_n = 3 \cdot 2^n + 4 \cdot 4^n + 3$

7. Kontroll: esiteks vaadata, kas vastus vastab algtingimustele (10, 25) ja siis arvutada ette antud rekurentse seose järgi välja veel üks liige ning vaadata, kas saadud vastu annab ka selle liikme

:::

::: details Näide erilahendi leidmise kohta

Olgu meil võrrand $a_n = 2a_{n-1} + 15a_{n-2} + 32 - 16n$ algtingimustega $a_0=8$ ja $a_1 =33$

Homogeense võrrandi lahendiks tuleb $a_n^{(h)} = c_15^n + c_2(-3)^n$.

Kuna vabaliikmes on sees $n$, siis ei ole tegemist konstantse jadaga. Otsime lahendit kujul $An+B$, paneme selle esialgsesse võrrandisse: $An+B=2(A(n-1)+B) + 15(A(n-2)+B) + 32 - 16n$

Peale lihtsustusi jääb alles $32A-16B-16An=32-16n$.

Selleks, et võrdus kehtiks iga $n$’i korral, peavad $n$’i ees olevad kordajad $-16$ ja $-16A$ olema võrdsed. Seega $A=1$ ja $B=0$ ning otsitav erilahend on $a_n = n$.

Üldlahend on seega  $a_n = c_15^n + c_2(-3)^n + n$.

:::

### Lahendamine genereerivate funktsioonide abil

## Tuntumad rekurrentsed jadad

### Aritmeetiline jada

$a_n = a_1 + d(n-1)$, kus $a_1$ on jada esimene liige ja $d$ on jada vahe/samm

### Geomeetriline jada

$a_n = a_1q^{n-1}$, kus $a_1$ on jada esimene liige ja $q$ on jada tegur

$S_n = a_1 \frac{q^n- 1}{q - 1}$, kus $S_n$ on esimese $n$ liikme summa

$S = a_1 \frac{1}{1-q}$, kus $S$ on hääbuva geomeetrilise jada summa

### Fibonacci jada

$F_n = F_{n-1} + F_{n-2} \text{ ; } F_0=0 \text{ , } F_1=1$


$$
F_n = \frac{1}{\sqrt{5}} (\frac{1 + \sqrt{5}}{2})^n -
\frac{1}{\sqrt{5}} (\frac{1 - \sqrt{5}}{2})^n = \frac{\phi^n-\widehat{\phi}^n	}{\sqrt{5}}
$$

genereeriv funktsioon: $f(n) = \frac{x}{1-x-x^2}$

::: details Genereeriva funktsiooni tuletamine

Teostame sellise lahutamistehte:
$$
\begin{aligned}
f(x) &\leftrightarrow <0,1, \space1,\space2,\space3,\space5,\space8,\space11, ...> \\
-xf(x) &\leftrightarrow <0,0,-1,-1,-2,-3,-5,-8, ...> \\
-x^2f(x) &\leftrightarrow <0, 0,0,-1,-1,-2,-3,-5, ...> \\
\hline 
f(x)-xf(x)-x^2f(x) &\leftrightarrow <0,1, \space0,\space0,\space0,\space0,\space0,\space0, ...>

\end{aligned}
$$

Kuna jada $<0,1,0,0,...>$ genereeriv funktsioon on $0x^0+1x^1+0x^2+...=x$, saame $f(x)-xf(x)-x^2f(x) = x \implies f(n) = \frac{x}{1-x-x^2}$.

:::

Lõigud $a$ ja $b$ on kuldlõikelises suhtes $\phi$, kui
$$
\frac{a+b}{a} = \frac{a}{b}
$$

$$
\begin{aligned}
\phi + \widehat{\phi} = 1 \\
\phi \cdot \widehat{\phi} = -1
\end{aligned}
$$



### Lucas’ jada

$L_n = L_{n-1} + L_{n-2} \text{ ; } L_0=2 \text{ , } L_1=1$

genereeriv funktsioon: $l(n) = \frac{2-x}{1-x-x^2}$
$$
\lim_{i\to\infty}\frac{F_n}{F_{n-1}} =
\lim_{i\to\infty}\frac{L_n}{L_{n-1}} = \phi
$$

### Catalani arvud

$$
\begin{aligned}
C_{n+1} &= \frac{2(2n+1)}{n+2}C_n \text{ ; } C_0=1 \\
C_n &= \frac{1}{n+1}\binom{2n}{n} 
= \sum_{k=0}^{n-1} C_k C_{n-1-k}\\
 \text{gen. funktsioon } C(x) &= \frac{1-\sqrt{1-4x}}{2x}

\end{aligned}
$$

Dycki keel – koosneb kahest tähest (näiteks `(` ja `)`), mida peab igas sõnes olema sama palju ning kus üheski prefiksis (sõne alguses paiknevas alamsõnes) ei ole lõpetavat sulgu rohkem kui algavat sulgu. Catalani arv $C_n$ on Dycki keele sõna, mille pikkus on kaks $2n$ tähte.

::: details üldliikme valemi tuletamine genereeriva funktsiooni abil

1. Pikendame Catalani arvuda jada ka negatiivses suunas Otsustame, et kõik negatiivse indeksiga jada liikemed on võrdsed nulliga.

2. Selliste arvujada saab esitada summana:
   $$
   C_n = \sum_k C_k C_{n-1-k} + [n=0]
   $$
   $[n=0]$ on Iversoni sulgude avaldis, mis võrbub ühega, kui sisu on tõene, vastasel juhul nulliga.

3.  Summast genereeriva funktsiooni leidmiseks tuleb iga jada liige korruada läbi vastava $x$‘i astmega:
   $$
   \begin{aligned}
   C(x) &= \sum_n C_n x^n = \\
   &= \sum_{n,k} C_k C_{n-1-k} x^n + \sum_n [n=0]x^n = \\
   &= \sum_{n,k} C_k C_{n-1-k} x^n + 1 = \\
   &= \sum_{k} C_k x^k x \sum_n C_{n-1-k} x^{n-1-k} + 1 = \\
   &= \sum_{k} C_k x^k x \sum_n C_n x^n + 1 = \\
   &= \sum_{k} C_k x^k x C(x) + 1 = \\
   &= x C(x)\sum_{k} C_k x^k  + 1 = \\
   &= x C(x)C(x) + 1 = \\
   &= x C^2(x) + 1 = \\
   \end{aligned}
   $$
   $\sum_{n,k}$ tähndab kahekordset summat: summa üle kõkide $n$’ide miinus lõpmatusest plus lõpmatuseni ning iga liikme jaoks veel summa üle kõkide $k$’de miinus lõpmatusest plus lõpmatuseni.

   $\sum_n [n=0]x^n = 1$, sest avaldise väärtus on 1 siis ja ainult siis  kui $n=0$.

   $\sum_n C_{n-1-k} x^{n-1-k} = \sum_n C_n x^n$ , sest kuna $n$ muutub miinus lõpmatusest plus lõpmatuseni, et mängi need mahalahutavad $-1-k$ mingit rolli.

4. Saame genereeriva funktsiooni kohta võrrandi:
   $$
   \begin{aligned}
   C(x) = xC^2(x) - C(x) + 1 = 0 \\
   C(x) = \frac{1 \pm \sqrt{1-4x}}{2x}
   \end{aligned}
   $$
   Plussmärgiga lahend on võõrlahend, sest kontroll näitab, et sellisel juhul hakkab $C(0)$ lähenema nullile (nimetajasse jääb $1+\sqrt{1}=2$ ja lugejasse jääb $0$). Miinusmärgiga lahend on sobilik, sinna jääb $C(0)$ korral $\frac{0}{0}$, mis võib võrduda üks kõik mis arvuga.

:::

::: details Genereeriva funktsiooni arendamine astmeritta

Lähtuvald binoomkordajate omadusest
$$
\sqrt{a+b} = (a+b)^\frac{1}{2}
= \sum_{k=0}^{\infty} \binom{\frac{1}{2}}{k} a^{\frac{1}{2}-k}b^k
= \sqrt{a}(1+\frac{b}{2a}\sum_{n=0}^{\infty}\frac{1}{(-4)^n(n+1)} \binom{2n}{n} (\frac{b}{a})^n)
$$

$$
\begin{aligned}
C(x) &= \frac{1 - \sqrt{1-4x}}{2x} \\
\sqrt{1-4x} &= 1-2x\sum_{n \geq 0}\frac{1}{n+1} \binom{2n}{n} x^n) \\
C(x)  &= \sum_{n \geq 0}\frac{1}{n+1} \binom{2n}{n} x^n) \\
C_n  &= \frac{1}{n+1} \binom{2n}{n}
\end{aligned}
$$


:::

## Binoomi teoreem

$$
\begin{aligned}
(a+b)^n &= \sum_{k=0}^n \binom{n}{k} a^{n-k}b^k = \\
&= \sum_{k=0}^\infty \binom{n}{k} a^{n-k}b^k 
\end{aligned}
$$

Binoomkordaja
$$
\begin{aligned}
\binom{n}{k}&=
\begin{cases}
k>n \to 0 \\
k=n \to 1 \\
k \leq n \to \frac{n!}{k!(n-k)!}=\frac{n(n-1)...(n-k+1)}{k!}
\end{cases} \\
\binom{n}{k} &= \frac{n}{k} \binom{n-1}{k-1} \\
\binom{n}{0} &= \binom{n}{n} =1
\end{aligned}
$$

::: details KaTex näited:
$$
\begin{aligned}
   a&=b+c \\
   d+e&=f
\end{aligned}
$$

$$
\begin{aligned}
         &1234\\
        &321\\
        &12345\\
        +&6\\
        \hline
        &13906
    \end{aligned}
$$

:::

