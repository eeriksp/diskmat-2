# Graafiteooria

vt http://www.targotennisberg.com/eio/VP/vp_ptk6_sissejuhatus_graafiteooriasse.pdf

## Definitsioon

### Definitsioon A

Graaf $G$ on struktuur $G = (V, E)$, kus

* $V = V(G)$ on tippude hulk (*vertex* mitmuses *vertices või ka *node*)
* $E = E(G) \subseteq V \times V$ on servade hulk (*edge*), mis koosneb järjestatud paaridest ja on hulga $V$ otsekorrutise alamhulk

### Definitsioon B

Graaf $G$ on struktuur $G = (V, E, \mathscr{E})$, kus

* $V = V(G)$ on tippude hulk
* $E = E(G)$
* $\mathscr{E}: E \to \mathscr{P}(V)$ on intsidentsusfunktsioon, nii, et iga $e \in E$ korral $|\mathscr{E}(e)| \in \{1,2\}$

Definitsioon B ei võimalda esitada suunatud graafe, sest servaga seonduvad tipub loetakse hulgast $\mathscr{E}(e)$, hulgas ei ole aga elemendid järjestatud ja seega ei saa neist üks olla algus ja teine lõpp-punkts.

Samas võimaldab definitsioon B kaks tippu ühendada rohkem kui ühe servaga. Deginitsioon A seda ei võimalda, sest $E$ on järjestatud paaride hulk, mille hulgas ei saa olla korduvaid elemente, seega võib olla kahe punkti vahel kaks eri suunaga serva, aga sama suunaga servi topelt olla ei saa.

## Servad

* Kui $\mathscr{E}(e) = \{u,v\}$, siis tippe $u$ ja $v$ nimetatakse serva $e$ **otstippudeks**. Tähistame, et $u \overset{e}{-} v$ või kui serva nimi pole oluline, siis $uv$. Ühe serva otstippe nimetatakse ka naabriteks.
* $e \in E$ on **kordne serv**, kui leidub serv $e' \in E \setminus\{e\}$ nii, et $\mathscr{E}(e) = \mathscr{E}(e’)$.
* **Silmus** on serv, mis algab ja lõpeb samas punktis.
* **Lihtgraaf** on graaf ilma kordsete servade ja silmusteta. 
  **Lihtgraaf on mittesuunatud.**
  * Olgu hulgal $V$ määratud sümmeetriline antireflektiivne binaarne seos  $E = \subseteq V \times V$, siis paar $E, V$ on lihtgraaf.

* **Suunatud graafi** ehk **orienteeritud graafi** ehk **o-graafi** servi nimetatakse ka **kaarteks**.

## Naabrusmaatriks

Olgu $G = (V, E)$ lihtgraaf tippudega $V=\{v_1, ..., v_i, ... ,v_j, ..., v_n\}$

Graafi $G$ naabrusmaatriks on $n \times n$ maatriks  $A=[a_{ij}]$, kus
$$
a =
\begin{cases}
1 \text{ kui } (v_i, v_j) \in E \\
0 \text{ kui } (v_i, v_j) \notin E
\end{cases}
$$
Näide: olgu graaf tippude hulgaga $E = \{v_1, v_2, v_3, v_4\}$
$$
\begin{array}{c|cccc}
& \pmb{v_1} & \pmb{v_2} & \pmb{v_3} & \pmb{v_4} \\
\hline
\pmb{v_1} & 0 & 1 & 1 & 0 \\
\pmb{v_2} & 1 & 0 & 1 & 1 \\
\pmb{v_3} & 1 & 1 & 0 & 1 \\
\pmb{v_4} & 0 & 1 & 1 & 0 \\
\end{array}
$$
Lihtgraafi naabrusmaatriks on peadeagonaali suhtes sümmeetriline (sest servade ei ole suunatud) ja peadiagonaalil on nullid (sest silmuseid ei ole).

Tipu $v \in V$ naabrite arvu $deg(v)$ nimetatakse tema astmeks ehk valentsiks. Naabrusmaatriksil võrdub tipu aste antud rea summaga (väiteks $v_1$ aste on $0+1+1+0=2$).

Teoreem: **Lihtgraafis on paarisarv paaritu astmega tippe.**

Kui tõsta naabrusmaatriks astmesse $n$, siis saame tabeli, kust on näha, kui palju teid kahe tipu vahel leidub, mille pikkus oleks $n$.

Kümnendsüsteemi arvu n esitamiseks kahendkujul on vaja maksimaalselt $log_2n$ bitti. 

## Teed, tsüklid, sidusus

* **Tee** ehk **ahel** on servada jada
  $$
  P: x_0 \overset{e_1}{-} x_1 \overset{e_2}{-} ...\overset{e_k}{-} x_k
  $$
  
* Tee pikkust tähistatakse $|P|$.
* Seda, et $P$ on tee tipust $x$ tippu $y$ tähistatakse $x \overset{P}{\leadsto} y$
* Kui tee algus- ja lõpp-punkt langevad kokku, on tegemist kinnise teega.
* Teed, kus kõik tipud on erinevad, nimetatakse lihtteeks, erandina võivad kokku langeda otspunktid $x_0$ ja $x_k$, sellisel juhul nimetatakse seda kinniseks lihtteeks ehk tsükliks.

* Kui graafi mistahes punktist saab jõuda mistahes teise punkti, on tegu sidusa graafiga (*connected graph*), mittesidusal graafil võib eristada sidusaid komponente.
* Graafi diameeter – üksteisest kõige kaugemal asuvate tippude vaheline kaugus. 

## Alamgraafid

Graafi $G=(V,E)$ alamgraafiks nimetatakse graafi $G'=(V',E')$ kui 

$V' \subseteq V$ ja $E' \subseteq E$ ja $\forall e \in E': \mathscr{E}(e) \subseteq V'$ (viimane tingimus tähendab, et iga alamgraafile ei saa valida servi, mille otspunkte graafile ei võeta).

Alamgraafi nimetatakse **indutseerituks**, kui alamgraafile on võetud kõik need servad, mis võetud tippudega seostuvad, ehk siis ei ole jäetud välja ühtegi serva, mille otstipud on olemas.

## Homomorfism

Võtame kaks graafi ja seame ühe graafi tippudega vastavusse teise graafi tipud (see vastavus ei ole olema ühene: neil kahel graafil võib olla erineva arv tippe ja seega võib ühele tipule vastata ka mitu teise graafi tippu). Kui esialgse graafi iga tippude paari puhul on nendega vastavusse seatud tipud omavahel naabrid parajasti siis, kui nad olid naabrid ja esialgsel graafil, on tegemist homomorfse graafiga.

Kui tippude-vaheline vastavus on üksühene, on graafid **isomorfsed** ja põhimõtteliselt on tegemist ühe ja sama graafiga. Isomorfism on homomorfismi erijuhtum.

## Täisgraaf

Täisgraaf on graaf, kus kõik tipud on omavahel servadega ühendatud.
Tähistatakse $K_n$, kus $n$ on tippude arv. Servade arv on $\frac{n(n-1)}{2}$.

## Nullgraaf

Nullgraaf on graaf, millel pole servi, tähistatakse $O_n$ või $N_n$.

## Kahealuselised graafid

Graaf on kahealuseline, kui selle tippude hulk on tükeldatav kaheks ühisosata hulgaks (aluseks) nii, et ühegi serva mõlemad otstipud ei kuulu samasse alusesse.

Kahealuselise täisgraafi puhul on mõlema aluse iga tipp ühendatud teise aluse iga tipuga, tähis $K_{m,n}$, kus $m$ ja $n$ on aluste tippude arvud. Servade arv on $mn$.

Graafi kromaatiline arv näitab, mitut värvi on vaja, et värvida ära iga tipp ära nii, et sama värvi tipud ei oleks kunagi kõrvuti. Kahealuselise graafi kromaatiline arv on kaks.

**Teoreem**: Graaf on kahealuseline parajasti siis, kui kõik tema tsüklid on paarisarvulise pikkusega.

## Euleri graaf

* **Euleri ahel** – ahel, mis läbib graafi iga serva täpselt üks kord.
* **Euleri graaf** – graaf, kus leidub kinnine Euleri ahel (lõpeb samas tipus, kust algas)
* **Euleri semigraaf** – graaf, kus leidub lahtine Euleri ahel (ei lõppe samas tipus, kust algas)

Kui graaf on sidus, siis järgmised väited on samaväärsed:

1. Graaf on Euleri graaf
2. Graafi kõikide tippude aste on paarisarv
3. Graafi saab jagada omavahel lõikumatuteks tsükliteks

Kui graafil on täpselt kahe tipu aste on paaritu, siis on tegemist Euleri semigraafiga.

## Hamiltoni graaf

* **Hamiltoni tsükkel** – läbib kõik graafi tipud täpselt üks kord
* **Hamiltoni ahel** – kinnine Hamiltoni tsükkel
* **Hamiltoni graaf** – graaf, kus leidub Hamiltoni tsükkel
* **Hamiltoni semigraaf** – graaf, kus leidub Hamiltoni ahel

Ei ole teada ühtegi lihtsat algoritmi, et leida, kas tegemist on Hamiltoni graafiga, see on NP keerukusega ülessanne.

## Puud

**Puu on sidus tsükliteta graaf.** Puu tippu, mille aste on üks, nimetatakse **leheks**. **Iga puu on kahealuseline graaf.** Puu tähis on enamasti $T$, mitte $G$

Tsükliteta graafi, mis ei ole sidus, nimetatakse metsaks.

Lemma: Kui graafil on $n$ tippu, $m$ serve ja $k$ sidususkomponenti, siis $n-k \leq m$.

Igas puus on $n-1$ serva.

Sidusa graafi serva nimetatakse **sillaks**, kui selle serva eemaldamisel graafi sidususkomponentide arv kasvab (graaf muutub mitte sidusaks).

Graaf on puu, kui ta on sidus ja iga tema serv on sild. Puus suvalise kahe tipu vahel leidub vaid üks lihttee.

## Graafi aluspuu

Sidusa graafi aluspuu (eng *span tree*) on tema selline alamgraaf, millel on olemas kõik graafi tipud (ära on jäätud kõik servad, mis ei ole sillad, kui veel üks serv ära jätta, siis ei oleks graaf enam sidus).

Olgu $G = (V, E, w)$ mingi n-tipuline kaalutud graaf, nii et iga serva $e \in E$ jaoks defineeritud kall $w(e) \in \mathbb{R}$. Graafi kaal on summa
$$
w(G) = \sum_{e \in E} w(e)
$$

### Minimaalse kaaluga aluspuu leidmine (Joseph Kruskal)

```python
n = len(V)  # `n` on tippude arv
edges = set()
for i in range(n-1):  # kokku on puus `n - 1` serva
	# Valida uus serv, mis
    # 1. ei ole veel valitud
    # 2. ei moodusta ühegi juba valitud servaga tsüklit
    # 3. on sobilike hulgast minimaalse kaaluga
    edges.add(edge)
```

### Aluspuu kasutamine rändkaupmehe ülesande lahendamisel

![image-20200426010133664](/home/sherab/.config/Typora/typora-user-images/image-20200426010133664.png)

Selline lahendus annab ideaalile lähedase tulemuse.

## Puude loendamine

**Cayley teoreem**: Märgendatud puid (ehk puid, kus tipudel on tähised ja on oluline, milline tipp kus asub) saab moodustada $n^{n-2}$ tükki (seega tekib jada 1, 1, 3, 16, 125 …).

## Puude esitamine arvuti mälus

### Naabrusmaatriks

vt ülalpool

### Servade loend

![image-20200426022842915](/home/sherab/.config/Typora/typora-user-images/image-20200426022842915.png)

Iga tipp figureerub Prüferi koodis üks kord vähem kui on tema aste (ülal näites tipu 1 aste on 1 ja ta fugureerib 1-1 korda, tipu 6 aste on kolm ja ta figureerib 3-1 korda). Kui kahel puul on sama Prüferi kood, siis nad on isomorfsed.

## Graafi värvimine

Graafi $G$ kromaatiline arv $\chi(G)$ näitab, mitut värvi on graafi tippude värvimiseks minimalselt vaja. $\chi'(G)$ näitab värvide arvu servade jaoks. Graafi servde värvimine on samaväärne tema servagraafi $E(G)$ tippude värvimisega (servagraafis on graafi servadest saanud graafi tipud). Servade värvimisel võib toimid ka nii: leida graafis maksimum kooskõla ning värvida kõik need servad ühte värvi. Korrata seda sama allesjäänud servade peal seni kaua, kuni kõik servad on värvitud.

**Graafi duaalne graaf** on graaf, kus esialgse graafi tahkudele vastavad tipud. Tipud on ühendatud seal, kus algse graafi tahud kokku puutusid. Näiteks siin pildil on roheine graf musta graafi dualne graaf:

<img src="/home/eeriksp/.config/Typora/typora-user-images/image-20200526193447611.png" alt="image-20200526193447611" style="zoom:50%;" />

Kui graaf on $k$ aluseline, saab seda värvida $k$ värviga.

$\Delta(G)$ – graafi tippude maksimaalne aste.

Graafi, milles ei ole ilmuseid, saab alati värvida $\Delta(G)+1$ värviga.

### Brooksi teoreem

Sidusat graafi, mis ei ole täisgraaf ega paarituarvuline lihttsükkel, värvimiseks ei ole kunagi vaja rohkem kui $\Delta(G)$ värvi.

### Nelja värvi probleem

Iga tasandiline graaf on värvitav nelja värviga.