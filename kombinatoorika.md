# Kombinatoorika

1. **permutatsioonid** (kui mitmel viisil saab n liikmelist hulka ümber järjestada):

   $P(n) = n!$

2. **variatsioonid** (ehk k-pemutatsioonid; kui mitmel viisil saab n-hulgast valida k liikmelise alamuhulga kui järjekord on oluline):

   $P(n,k) = \frac{n!}{(n-m!)}$

3. **kombinatsioonid** (kui mitmel viisil saab n-hulgast valida k liikmelise alamuhulga kui järjekord ei ole oluline):

   $C(n,k) = \binom{n}{k} = \frac{n!}{k!(n-m!)}$

4. **kordustega permutatsioonid (lõputult palju valikuid)** (mitmel viisil saab n-hulgast moodustada m pikkusega järjendi, kui ühte ja sama elementi võib n-hulgast võtta piiramatu arv kordi):

   $W(n,m) = n^m$

5. **kordustega permutatsioonid (piiratud valikud)** (näiteks kui mitmel moel saab ümber järjestada tähti sõnas “RODODENDRON”)

   $\binom{n}{n_1, n_2, ... n_k} = \frac{n!}{n_1!, n_2!, ... n_k!}$

   kus $n$ on elementide koguarv, $n_k$ on kindlat liiki elementide arv